/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myutils;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author alumneDAM
 */
public class MyUtilsTest {
    
    public MyUtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

  
    @Test
    public void testInverteix() {
        System.out.println("inverteix");
        MyUtils mu = new MyUtils();
        
        String[] cads = {"Hola Mundo", null};
        String[] res = {"odnuM aloH", null};
        
        for (int cont=0;cont<3;cont++){
        assertEquals(MyUtils.inverteix(cads[cont]), res[cont]);
        }
    }

    /**
     * Test of edat method, of class MyUtils.
     */
    @Test
    @Ignore
    public void testEdad() {
        System.out.println("Edad:");
        int day = 0;
        int month = 0;
        int year = 0;
        int expResult = 0;
        int result = MyUtils.edad(day, month, year);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of factorial method, of class MyUtils.
     */
    @Test
    @Ignore
    public void testFactorial() {
        System.out.println("Factorial:");
     MyUtils util=new MyUtils();
     double[] num = {0,-1,4};
     double[] res= {1,-1,24};
     for  (int con=0; con<3; con++){
         assertTrue(MyUtils.factorial(num[con])==res[con]);
     }
     
    }

 
}
